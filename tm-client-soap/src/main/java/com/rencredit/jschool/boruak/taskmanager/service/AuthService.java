package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthService {

    @Autowired
    private AuthEndpoint authEndpoint;

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Autowired
    private UserEndpoint userEndpoint;

    @Autowired
    private AdminEndpoint adminEndpoint;

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private SessionService sessionService;

    public boolean login(final String login, final String password) throws EmptyLoginException_Exception, UnknownUserException_Exception, DeniedAccessException_Exception {
        SessionService.setMaintain(authEndpoint);
        SessionService.setMaintain(projectEndpoint);
        SessionService.setMaintain(taskEndpoint);
        SessionService.setMaintain(userEndpoint);
        SessionService.setMaintain(adminEndpoint);
        SessionService.setMaintain(adminUserEndpoint);

        if(authEndpoint.login(login, password).isSuccess()){
            List<String> session = sessionService.getSession();
            if(session == null) {
                session = SessionService.getListCookieRow(authEndpoint);
                sessionService.setSession(session);
            }
            SessionService.setListCookieRowRequest(authEndpoint, session);
            SessionService.setListCookieRowRequest(projectEndpoint, session);
            SessionService.setListCookieRowRequest(taskEndpoint, session);
            SessionService.setListCookieRowRequest(userEndpoint, session);
            SessionService.setListCookieRowRequest(adminEndpoint, session);
            SessionService.setListCookieRowRequest(adminUserEndpoint, session);
            return true;
        } else {
            return false;
        }
    }

    public void logout() {
        SessionService.setListCookieRowRequest(authEndpoint, new ArrayList<>());
        SessionService.setListCookieRowRequest(projectEndpoint, new ArrayList<>());
        SessionService.setListCookieRowRequest(taskEndpoint, new ArrayList<>());
        SessionService.setListCookieRowRequest(userEndpoint, new ArrayList<>());
        SessionService.setListCookieRowRequest(adminEndpoint, new ArrayList<>());
        SessionService.setListCookieRowRequest(adminUserEndpoint, new ArrayList<>());
        authEndpoint.logout();
    }

}

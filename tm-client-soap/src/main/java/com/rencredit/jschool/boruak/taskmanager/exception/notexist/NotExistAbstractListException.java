package com.rencredit.jschool.boruak.taskmanager.exception.notexist;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class NotExistAbstractListException extends AbstractClientException {

    public NotExistAbstractListException() {
        super("Error! Abstract list is not exist...");
    }

}

package com.rencredit.jschool.boruak.taskmanager.listener;

//import com.rencredit.jschool.boruak.taskmanager.api.service.ISystemObjectService;

import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.event.ConsoleEvent;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyProjectException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptySessionException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import org.springframework.stereotype.Component;

@Component
public abstract class AbstractListener {

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public Role[] roles() {
        return null;
    }

    public abstract void handle(final ConsoleEvent event) throws EmptyUserException, EmptySessionException, EmptyTaskException, EmptyProjectException, EmptyCommandException, EmptyLoginException_Exception, EmptyUserIdException_Exception, EmptyProjectException_Exception, EmptyIdException_Exception, EmptyTaskException_Exception, UnknownUserException_Exception, DeniedAccessException_Exception, NotExistUserException_Exception, EmptyRoleException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, EmptyPasswordException_Exception, IncorrectHashPasswordException_Exception, EmptyNewPasswordException_Exception, EmptyProjectIdException_Exception;

    public AbstractListener() {
    }

}

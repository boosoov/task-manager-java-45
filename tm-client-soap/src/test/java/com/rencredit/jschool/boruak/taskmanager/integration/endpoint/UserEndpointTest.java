package com.rencredit.jschool.boruak.taskmanager.integration.endpoint;

import com.rencredit.jschool.boruak.taskmanager.config.ClientConfiguration;
import com.rencredit.jschool.boruak.taskmanager.endpoint.soap.*;
import com.rencredit.jschool.boruak.taskmanager.marker.IntegrationWithServerTestCategory;
import com.rencredit.jschool.boruak.taskmanager.service.AuthService;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ClientConfiguration.class)
@Category(IntegrationWithServerTestCategory.class)
public class UserEndpointTest {

    @Autowired
    private UserEndpoint userEndpoint;

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private AuthService authService;

    @Before
    public void init() throws EmptyLoginException_Exception, DeniedAccessException_Exception, UnknownUserException_Exception, EmptyPasswordException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, EmptyRoleException_Exception, BusyLoginException_Exception {
        authService.login("admin", "admin");
        adminUserEndpoint.clearAllUser();
        authService.login("admin", "admin");
    }

    @After
    public void clearAll() throws DeniedAccessException_Exception, EmptyHashLineException_Exception, EmptyRoleException_Exception, EmptyLoginException_Exception, EmptyPasswordException_Exception, BusyLoginException_Exception, EmptyUserException_Exception, UnknownUserException_Exception {
        authService.login("admin", "admin");
        adminUserEndpoint.clearAllUser();
    }

    @Test
    public void testUpdateUserPassword() throws EmptyUserIdException_Exception, UnknownUserException_Exception, EmptyRoleException_Exception, IncorrectHashPasswordException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, EmptyHashLineException_Exception, NotExistUserException_Exception, EmptyNewPasswordException_Exception, EmptyLoginException_Exception {
        userEndpoint.updateUserPassword("newPassword");
        authService.logout();
        assertTrue(authService.login("admin", "newPassword"));
        userEndpoint.updateUserPassword("admin");
    }

    @Test
    public void testUpdateUser() throws UnknownUserException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyUserException_Exception, NotExistUserException_Exception, EmptyLoginException_Exception, BusyLoginException_Exception {
        final UserDTO user = userEndpoint.getMyUser();
        user.setEmail("mail");
        userEndpoint.updateUser(user);
        final UserDTO userWithChange = userEndpoint.getMyUser();
        assertEquals("mail", userWithChange.getEmail());
    }

    @Test
    public void testGetMyUser() throws UnknownUserException_Exception, DeniedAccessException_Exception, EmptyIdException_Exception, EmptyLoginException_Exception {
        authService.login("test", "test");
        final UserDTO user = userEndpoint.getMyUser();
        assertEquals("test", user.getLogin());
    }

}

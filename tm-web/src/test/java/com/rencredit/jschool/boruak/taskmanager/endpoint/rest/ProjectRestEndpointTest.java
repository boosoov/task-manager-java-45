package com.rencredit.jschool.boruak.taskmanager.endpoint.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.rencredit.jschool.boruak.taskmanager.config.WebApplicationConfiguration;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Project;
import com.rencredit.jschool.boruak.taskmanager.entity.User;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WebApplicationConfiguration.class)
public class ProjectRestEndpointTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserService userService;

    @Autowired
    private ProjectService projectService;

    private MockMvc mockMvc;

    private User admin;

    @Before
    public void setup() throws EmptyLoginException, EmptyUserException, BusyLoginException, DeniedAccessException, EmptyPasswordException, EmptyRoleException, EmptyHashLineException {
        this.mockMvc = MockMvcBuilders
                        .webAppContextSetup(this.webApplicationContext)
                        .build();

        SecurityContext ctx = SecurityContextHolder.createEmptyContext();
        SecurityContextHolder.setContext(ctx);
        ctx.setAuthentication(new UsernamePasswordAuthenticationToken("admin", "", Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"))));
        userService.deleteAll();
        admin = userService.findByLoginEntity("admin");

        final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("admin", "admin");
        final Authentication authentication =
                authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    @Transactional
    public void testCreate() throws Exception {
        final Project project = new Project();
        project.setName("project");
        project.setDescription("project");
        project.setUser(admin);
        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        final ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(project);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                                .post("/api/project")
                                .contentType(MediaType.APPLICATION_JSON_UTF8)
                                .content(requestJson));

//        result.andDo(print());
        result.andExpect(MockMvcResultMatchers.status().isOk());
        final ProjectDTO projectFromBase = projectService.findByIdDTO(admin.getId(), project.getId());
        assertEquals(project.getName(), projectFromBase.getName());
    }

    @Test
    public void testFindOneByIdDTO() throws Exception {
        final Project project = new Project();
        project.setName("projectName");
        project.setDescription("projectDescription");
        project.setUser(admin);
        projectService.save(admin.getId(), project);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/project/{id}", project.getId())
                .accept(MediaType.APPLICATION_JSON));

//        result.andDo(print());
        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name").value(project.getName()))
                .andExpect(jsonPath("$.description").value(project.getDescription()));

    }

    @Test
    public void testExistsById() throws Exception {
        final Project project = new Project();
        project.setName("projectName");
        project.setDescription("projectDescription");
        project.setUser(admin);
        projectService.save(admin.getId(), project);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/project/exist/{id}", project.getId())
                .accept(MediaType.APPLICATION_JSON));

//        result.andDo(print());
        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("true"));

        result = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/project/exist/{id}", "dfsdfsd")
                .accept(MediaType.APPLICATION_JSON));

//        result.andDo(print());
        result.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().string("false"));
    }

    @Test
    public void testDeleteOneById() throws Exception {
        final Project project = new Project();
        project.setName("projectName");
        project.setDescription("projectDescription");
        project.setUser(admin);
        projectService.save(admin.getId(), project);
        assertNotNull(projectService.findByIdDTO(admin.getId(), project.getId()));

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders
                .delete("/api/project/{id}", project.getId())
                .accept(MediaType.APPLICATION_JSON));

//        result.andDo(print());
        result.andExpect(MockMvcResultMatchers.status().isOk());
        assertNull(projectService.findByIdDTO(admin.getId(), project.getId()));
    }

}

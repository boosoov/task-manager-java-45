package com.rencredit.jschool.boruak.taskmanager.service;

import com.rencredit.jschool.boruak.taskmanager.dto.CustomUser;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

@Service("userDetailsService")
@Transactional
public class UserDetailServiceBean implements UserDetailsService {

    @Autowired
    UserService userService;

    @Override
    @SneakyThrows
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        @Nullable final UserDTO user = userService.findByLoginDTO(userName);
        if(user == null) throw new NotExistUserException();
        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(new String[]{user.getRole().getDisplayName()})
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    @Transactional
    public void init() throws EmptyHashLineException, BusyLoginException, EmptyPasswordException, EmptyLoginException, EmptyUserException, DeniedAccessException, EmptyRoleException {
//        if(!userService.findListDTO().isEmpty()) return;
//        userService.save("1", "1");
//        userService.save("test", "test");
//        userService.save("admin", "admin", Role.ADMIN);
    }

}

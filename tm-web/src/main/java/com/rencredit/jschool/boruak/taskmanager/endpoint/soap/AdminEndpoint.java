package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.soap.IAdminEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.service.IAuthService;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Role;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyRoleException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.PropertyService;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * http://localhost:8080/services/AdminEndpoint?wsdl
 */

@Component
@WebService
public class AdminEndpoint implements IAdminEndpoint {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    public AdminEndpoint() {
    }

    @Override
    @WebMethod
    public String getHostPort()  {
        return "HOST:" + propertyService.getServerHost() + "\n" + "PORT:" + propertyService.getServerPort();
    }

    @Override
    @WebMethod
    public String getHost() {
        System.err.println(propertyService.getServerHost());
        return propertyService.getServerHost();
    }

    @Override
    @WebMethod
    public Integer getPort() {
        return propertyService.getServerPort();
    }

    @Override
    @WebMethod
    public void shutDownServer() {
        System.exit(0);
    }

}


package com.rencredit.jschool.boruak.taskmanager.controller;

import com.rencredit.jschool.boruak.taskmanager.dto.CustomUser;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.enumerated.Status;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyNameException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import com.rencredit.jschool.boruak.taskmanager.service.UserService;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private UserService userService;

    @ModelAttribute("statuses")
    public Status[] getStatus() {
        return Status.values();
    }

    @ModelAttribute("projects")
    private List<ProjectDTO> getProjects() throws DeniedAccessException, UnknownUserException, EmptyUserIdException {
        return projectService.findAllByUserIdDTO(UserUtil.getUserId());
    }

    @GetMapping("/task/create")
    public String create(
            @AuthenticationPrincipal CustomUser user
    ) throws EmptyNameException, EmptyUserIdException {
        taskService.save(user.getUserId(), "Empty");
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @PathVariable("id") String id,
            @AuthenticationPrincipal CustomUser user
    ) throws EmptyIdException, EmptyUserIdException {
        taskService.deleteById(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @PathVariable("id") String id,
            @AuthenticationPrincipal CustomUser user
    ) throws EmptyIdException, EmptyUserIdException {
        final TaskDTO task = taskService.findOneByIdDTO(user.getUserId(), id);
        return new ModelAndView("task/task-edit", "task", task);
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @ModelAttribute("task") TaskDTO task,
            BindingResult result,
            @AuthenticationPrincipal CustomUser user
    ) throws EmptyUserIdException, EmptyTaskException {

        if (task.getProject() == null || task.getProject().isEmpty()) task.setProject(null);
        task.setUserId(user.getUserId());
        taskService.save(task);
        return "redirect:/tasks";
    }

}

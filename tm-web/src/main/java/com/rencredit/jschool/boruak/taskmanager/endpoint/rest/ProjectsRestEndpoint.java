package com.rencredit.jschool.boruak.taskmanager.endpoint.rest;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.rest.IProjectsRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyProjectListException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectsRestEndpoint implements IProjectsRestEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @GetMapping
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public List<ProjectDTO> getListDTO() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        return projectService.findAllByUserIdDTO(UserUtil.getUserId());
    }

    @Override
    @GetMapping("/all")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public List<ProjectDTO> getListDTOAll() {
        return projectService.findList();
    }

    @Override
    @PostMapping
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public List<ProjectDTO> saveAll(@RequestBody List<ProjectDTO> list) throws DeniedAccessException, UnknownUserException, EmptyProjectListException {
        for (ProjectDTO project : list) {
            project.setUserId(UserUtil.getUserId());
        }
        return projectService.saveList(list);
    }

    @Override
    @GetMapping("/count")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public long count() throws DeniedAccessException, UnknownUserException, EmptyUserIdException {
        return projectService.countAllByUserId(UserUtil.getUserId());
    }

    @Override
    @DeleteMapping("/all")
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void deleteAll() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        projectService.deleteAllByUserId(UserUtil.getUserId());
    }

}

package com.rencredit.jschool.boruak.taskmanager.endpoint.soap;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.soap.IUserEndpoint;
import com.rencredit.jschool.boruak.taskmanager.api.service.IUserService;
import com.rencredit.jschool.boruak.taskmanager.dto.UserDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.busy.BusyLoginException;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectHashPasswordException;
import com.rencredit.jschool.boruak.taskmanager.exception.notexist.NotExistUserException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * http://localhost:8080/services/UserEndpoint?wsdl
 */

@Component
@WebService
public class UserEndpoint implements IUserEndpoint {

    @Nullable
    @Autowired
    private IUserService userService;

    public UserEndpoint() {
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO updateUserPassword(
            @Nullable @WebParam(name = "newPassword", partName = "newPassword") String newPassword
    ) throws EmptyUserException, IncorrectHashPasswordException, EmptyIdException, EmptyNewPasswordException, EmptyHashLineException, DeniedAccessException, UnknownUserException, NotExistUserException, EmptyRoleException, EmptyUserIdException {
        return userService.updatePasswordById(UserUtil.getUserId(), newPassword);
    }

    @Override
    @WebMethod
    public void updateUser(
            @Nullable @WebParam(name = "user", partName = "user") UserDTO user
    ) throws EmptyUserException, EmptyIdException, DeniedAccessException, UnknownUserException, NotExistUserException, EmptyLoginException, BusyLoginException {
        UserDTO userFromBase = userService.findByIdDTO(UserUtil.getUserId());
        if(user == null || userFromBase == null) throw new NotExistUserException();
        if(!user.getId().equals(userFromBase.getId())) throw new UnknownUserException();
        userService.rewrite(user);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO getMyUser() throws DeniedAccessException, UnknownUserException, EmptyIdException {
        return userService.findByIdDTO(UserUtil.getUserId());
    }

}
package com.rencredit.jschool.boruak.taskmanager.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
public final class Domain implements Serializable {

    public static final long serialVersionUID = 1L;

    @NotNull
    private List<ProjectDTO> projects = new ArrayList<>();

    @NotNull
    private List<TaskDTO> tasks = new ArrayList<>();

    @NotNull
    private List<UserDTO> users = new ArrayList<>();

    public void setProjects(@Nullable final List<ProjectDTO> projects) {
        this.projects = projects;
    }

    public void setTasks(@Nullable final List<TaskDTO> tasks) {
        this.tasks = tasks;
    }

    public void setUsers(@Nullable final List<UserDTO> users) {
        this.users = users;
    }
}

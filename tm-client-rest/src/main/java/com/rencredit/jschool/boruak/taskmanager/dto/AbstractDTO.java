package com.rencredit.jschool.boruak.taskmanager.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
public class AbstractDTO implements Serializable {

    public static final long serialVersionUID = 1L;

    @NotNull
    private String id = UUID.randomUUID().toString();

}
